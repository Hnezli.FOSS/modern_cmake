#!/bin/bash

TARGET=$1
BUILDDIR=$2


if [ "$#" -ne 2 ]; then
    echo "Usage: build.sh <target> <builddir>"
    exit -1
fi


cmake -S . -B ${BUILDDIR} # For windows add:  -G "MinGW Makefiles"
cmake --build ${BUILDDIR}

cd ${BUILDDIR}
cmake --build . --target ${TARGET}
cd -
