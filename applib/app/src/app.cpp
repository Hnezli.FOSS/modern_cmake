

#include <iostream>
#include "lib/inc/lib.hpp"
#include "inc/app.hpp"


int main(void) {
    std::cout <<'[' <<__FILE__ <<", " <<__LINE__ <<']'
	      <<"hello from app::main"
	      <<'\n';

    Lib lib;

    lib.foo();
    bar();

    return 0;
}

void bar(void) {
    std::cout <<'[' <<__FILE__ <<", " <<__LINE__ <<']'
              <<"hello from app::bar"
              <<'\n';

    return;
}
