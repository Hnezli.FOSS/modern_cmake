#!/bin/bash


# setup ########################################################################
BUILD_SCRIPT_DIR=$(dirname -- "${BASH_SOURCE[0]}")
source ${BUILD_SCRIPT_DIR}/global-vars.def

unset TARGET_NAME


# arguments parsing ############################################################
function print_usage {
    #echo "Usage: build.sh <optional: target> <optional: builddir>"
    echo "$(basename $0) [OPTIONS]"
    echo '    -h                           show usage'
    echo '    -b,--build-dir  <build dir>  set <build_dir> default is "build"'
    echo '    -a,--app <target>            build application <target> only'
    echo '    -c                           cleans up every thing before building'
    echo '    --test <test report dir>     build tests instead only'
    echo '    --doc <doc dir>              build doxgen docs'
}


function parse_args {
    SHORT_OPTS=":hb:a:c"
    LONG_OPTS="builddir:,app:,test:,doc:"
    TEMP=`getopt -o ${SHORT_OPTS} --long ${LONG_OPTS} -- "$@"`
    if [ $? -ne 0 ]; then
        echo 'Incorrect option provided'
        echo 'Please see --help for usage details'
        ${BUILD_SCRIPT_DIR}/exit_if_error.sh 1
    fi
    eval set -- "$TEMP"

    while true ; do
        case "$1" in
        -h)
            print_usage
            exit 0
            ;;
        -b|--build-dir)
            export BUILD_DIR=$2
            shift 2;;
        -a|--app)
            export TARGET_NAME=$2
            shift 2;;
        -c)
            export CLEAN_BUILD="true"
            shift 1;;
        --test)
            export TARGET_NAME=test
            export MODERN_CMAKE_BUILD_TESTING='true'
            export TEST_REPORT_DIR=${WORK_DIR}/$2
            shift 2;;
        --doc)
            export TARGET_NAME=doc
            export DOC_DIR=${WORK_DIR}/$2
            shift 2;;
        --)
            shift
            break
            ;;
        *)
           echo "Invalid option: $2"
           ${BUILD_SCRIPT_DIR}/exit_if_error.sh 1
           ;;
        esac
    done
}



# main #########################################################################
parse_args $@
${BUILD_SCRIPT_DIR}/exit_if_error.sh  $?

# clean up
if [ "${CLEAN_BUILD}" == "true" ]; then
    echo 'Clean build wird verlangt'
    rm -rf ${BUILD_DIR}
fi

# build FW/test/doc
cmake -S ${BASE_DIR} -B ${BUILD_DIR} # For windows add:  -G "MinGW Makefiles"
case "${TARGET_NAME}" in
smartbattery)
    # build application
    echo "building app ${TARGET_NAME}"
    cmake --build ${BUILD_DIR} --target ${TARGET_NAME}
    ${BUILD_SCRIPT_DIR}/exit_if_error.sh $?
    make install -C ${BUILD_DIR}
    ${BUILD_SCRIPT_DIR}/exit_if_error.sh $?
    ;;

test)
    # run tests
    echo "tests requested, reports will be saved in ${TEST_REPORT_DIR}"
    mkdir -p ${TEST_REPORT_DIR}
    ${BUILD_SCRIPT_DIR}/exit_if_error.sh $?
    ;;

doc)
    # build documentation
    echo 'building docs'
    #mkdir -p ${DOC_DIR}
    cmake --build ${BUILD_DIR} --target ${TARGET_NAME}
    ${BUILD_SCRIPT_DIR}/exit_if_error.sh $?
    ;;
*)
    echo -e "target \"${TARGET_NAME}\" is not supported!"
    ${BUILD_SCRIPT_DIR}/exit_if_error.sh $1
    ;;
esac


exit 0


