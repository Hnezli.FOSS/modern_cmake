

# tested with 3.24. Also should Works with 3.12
cmake_minimum_required(VERSION 3.12...3.24)

# load global config
set(CMAKE_MODULE_PATH
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake"
    ${CMAKE_MODULE_PATH}
)
include(global-config)

# load toochain
find_file(
    CMAKE_TOOLCHAIN_FILE
    toolchain-linux-gcc-x86_64x.cmake
    ${CMAKE_MODULE_PATH}
)
if(NOT DEFINED CMAKE_TOOLCHAIN_FILE)
    message(FATAL_ERROR "toolchain file not found!")
endif()


# project ######################################################################
# project attributes
project(
    smartbattery
    VERSION        0.1.0
    DESCRIPTION    "smart expandable battery system"
    LANGUAGES      CXX
)


# Only do these if this is the main project, and not if it is included through add_subdirectory
if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    # Optionally set things like CMAKE_CXX_STANDARD, CMAKE_POSITION_INDEPENDENT_CODE here
    set(CMAKE_BUILD_TYPE "Debug")
    set(BUILD_SHARED_LIBS OFF)

    # Let's ensure -std=c++xx instead of -std=g++xx
    set(CMAKE_CXX_EXTENSIONS OFF)

    # Docs only available if this is the main app
    find_package(Doxygen)
    if(Doxygen_FOUND)
        message(STATUS "TODO: add doxygen")
        add_subdirectory(doc)
    else()
        message(STATUS "Doxygen not found, not building docs")
    endif()
endif()


# target #######################################################################
set(TARGET_NAME ${PROJECT_NAME})
set(HEADER_FILES
    inc/smartbattery/smartbattery.hpp
)
set(SOURCE_FILES
    src/smartbattery.cpp
    src/main.cpp
)
add_executable(${TARGET_NAME} ${SOURCE_FILES} ${HEADER_FILES})

# export path 
target_include_directories(${TARGET_NAME} PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc/${TARGET_NAME}>
    $<INSTALL_INTERFACE:inc/${TARGET_NAME}>
)


# lib ##########################################################################
set(TARGET_LIBRARIES_DIR ${PROJECT_LIBRARIES_DIR})
set(TARGET_LIBRARIES
  powercell
)
foreach(LIBRARY ${TARGET_LIBRARIES})
    add_subdirectory("${TARGET_LIBRARIES_DIR}/${LIBRARY}")
endforeach(LIBRARY)
target_link_libraries(${TARGET_NAME} ${TARGET_LIBRARIES})


# test #########################################################################
# run test if this is the main project or requested by root CMakeLists
# Emergency override MODERN_CMAKE_BUILD_TESTING provided as well
if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME) OR MODERN_CMAKE_BUILD_TESTING)
    message(STATUS "TODO: Don't forget to enable testing")
    include(CTest)
    #add_subdirectory(test)
    foreach(LIBRARY ${TARGET_LIBRARIES})
        #add_subdirectory("${TARGET_LIBRARIES_DIR}/${LIBRARY}/test")
    endforeach(LIBRARY)
endif()


# install ######################################################################
install(
    TARGETS ${TARGET_NAME}
)


