

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

set(CMAKE_SYSTEM_VERSION 1)


# compiler
set(CMAKE_CXX_COMPILER      g++)
#set(CMAKE_CROSSCOMPILING    TRUE)
#set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(CMAKE_EXECUTABLE_SUFFIX_CXX .bin)

# location of target environment
set(CMAKE_FIND_ROOT_PATH  /usr/bin)

# search for programs in the build host directories
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)


add_compile_options(
    -march=x86-64

    -static

    -ffloat-store

    -ffunction-sections
    -fdata-sections
    -fno-exceptions
)
add_link_options(
    -march=x86-64

    -lstdc++
    -lsupc++

    -Wl,-Map=mem.map
    -Wl,--gc-sections
)

