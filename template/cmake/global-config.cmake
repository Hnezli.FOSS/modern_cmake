

################################################################################
# please include this configuration file at the very root level CMake file
################################################################################

# setting
execute_process (
    COMMAND bash -c "nproc"
    OUTPUT_VARIABLE CMAKE_BUILD_PARALLEL_LEVEL
)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)                  # support folders in IDEs

set(PROJECT_LIBRARIES_DIR         lib)
set(BUILD_TESTING                 OFF)                        # default is OFF

set(DL_DIR                        ${CMAKE_BINARY_DIR}/../dl)  # downloaded files are saved here

set(CMAKE_INSTALL_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/../install)
set(CMAKE_INSTALL_BINDIR    bin)
set(CMAKE_INSTALL_LIBDIR    lib)
set(CMAKE_INSTALL_DOCDIR    doc)

SET(DOXYGEN_INPUT_DIR ${CMAKE_PROJECT_SOURCE_DIR})
SET(DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/../doc)


