# modern_cmake



## Build instructions
Run these commands in the relevant directory

```
BUILDDIR=build
TARGET=app                # set this to the desired target

cmake -S . -B ${BUILDDIR} # For windows add:  -G "MinGW Makefiles"
cmake --build ${BUILDDIR}

```
